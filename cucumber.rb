require 'fileutils'
module Cucumber
  class Cmd
    def initialize(cmd, suboption)
      @cmd = cmd
      @suboption = suboption
      @suboption_close = suboption.empty? ? '' : '\''
      @opts = []
      @subopts = []
      @video_tweak_trick = ->(_, _) {}
      @device_filters = ''
    end

    attr_reader :video_tweak_trick

    def self.parallel_cucumber(options)
      new('parallel_cucumber', '--cucumber-options \'')
        .subopt("BMA_SERVER=#{options[:bma_server]}")
        .subopt('--format pretty --no-color --profile parallel_cucumber_reports')
        .subopt("CAPTURE_VIDEO=#{options[:capture_video]}")
    end

    def self.cucumber(options)
      new('cucumber', '')
        .opt('--format rerun --out rerun.txt')
        .opt('--format junit --out build/reports')
        .opt('--format pretty')
        .opt('--format html --out cukes.html')
    end

    def opt(opt)
      @opts << opt
      self
    end

    def device_filters(device_filters)
      @device_filters = device_filters
      self
    end

    def subopt(opt)
      @subopts << opt
      self
    end

    def video_tweak(tweak)
      @video_tweak_trick = tweak
      self
    end

    def tags_or_empty(test_filters)
      test_filters.empty? || test_filters.include?('-t ') || test_filters.include?(' --tags')
    end

    def filter_scenario_clause(test_filters, scenario = 'features/')
      if tags_or_empty(test_filters)
        " #{test_filters}#{@suboption_close} #{scenario}"
      else
        "#{@suboption_close} #{test_filters}"
      end
    end

    def complete_with(test_filters, scenario = 'features/')
      partial = ['bundle exec', @cmd, @opts, @device_filters, @suboption, @subopts].flatten.join(' ')
      partial + filter_scenario_clause(test_filters, scenario)
    end
  end

  class << self
    def run_rerun_parallel_cucumber(options, part)
      run_rerun_function(options, part) do |cmd, dir|
        execute_cucumber_cmd(cmd, dir) { |d| merge_parallel_reports(d) }
      end
    end

    def run_rerun_single_cucumber(options, part)
      run_rerun_function(options, part) do |cmd, dir|
        execute_cucumber_cmd(cmd, dir)
      end
    end

    private

    def failed_scenarios(files)
      failed_scenarios = nil

      Dir.glob(files).each do |file|
        scenarios = File.read(file).chomp
        unless scenarios.empty?
          if failed_scenarios.nil?
            failed_scenarios = scenarios
          else
            failed_scenarios << ' '
            failed_scenarios << scenarios
          end
        end
        FileUtils.rm(file)
      end

      if failed_scenarios.nil?
        puts '*** No test cases failed'
      else
        puts "**** Failed test cases: #{failed_scenarios.count(':')} ****"
        puts failed_scenarios
        puts '********'
      end
      failed_scenarios
    end

    def merge_parallel_reports(reports_dir)
      FileUtils.mv(Dir.glob('junit_*/'), reports_dir, verbose: true, force: true)
      Merge.merge_cukes_report(reports_dir)
    end

    def execute_cucumber_cmd(command, reports_dir)
      system %( rm -rf #{reports_dir}/* )
      FileUtils.mkdir_p(reports_dir)
      run_name = reports_dir.split('/').last

      Teamcity.block("cucumber #{run_name}") do
        puts %( #{command}; true )
        system %( #{command}; true )
      end

      FileUtils.mv(Dir.glob(CALABASH_ARTIFACT_GLOB), reports_dir, verbose: true, force: true)

      yield(reports_dir) if block_given?

      failed_scenarios('rerun*txt')
    end

    def all_scenarios_passed(scenarios)
      scenarios.nil? || scenarios.empty?
    end
  end
end
