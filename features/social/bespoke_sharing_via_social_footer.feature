@JIRA-AM-15 @android
Feature: Bespoke sharing via Social footer
**Description**:
In order to share Bespoke
As a blippar user
I want to verify the behaviour app sharing on Facebook ,Twitter and Email.

**Preconditions**:
Should be within a Blipp using Bespoke Sharing mode
Should have the Facebook, Twitter and Email applications  installed and logged in on  devices


  Scenario Outline: Share the Bespoke blipp  via Facebook and Twitter
    Given I trigger the "Guardians of galaxy" blipp
    Then  I verify blipp "Guardians of galaxy" is loaded successfully
    When I tap on the "<share>" button
    Then I should see all the icons Facebook, Twitter and Email
    When I select "<social_icon>" icon
    Then I verify "<social_icon>" share pop up appears
    When I fill "<shared_text>" in pre-fill box of "<social_icon>" share pop up
    And  I tap on Post button of "<social_icon>" share pop up
    Then I verify blipp is shared on "<social_icon>"

    Examples:
      | Scenario                                                          | social_icon | share       | shared_text                                    |
      | Bespoke photo sharing on Facebook with shared text  on Facebook   | Facebook    | Share Blipp | This is Guardians of galaxy blipp photo shared |
      | Bespoke photo sharing on Facebook without shared text on Facebook | Facebook    | Share Blipp |                                                |
      | Bespoke photo sharing on Facebook with shared text on Twitter     | Twitter     | Share Blipp | This is Guardians of galaxy blipp photo shared |
      | Bespoke photo sharing on Facebook without shared text on Twitter  | Twitter     | Share Blipp |                                                |

  Scenario: Verify sharing of photo via Instagram
    Given I trigger the "Guardians of galaxy" blipp
    When I tap on the "Share Photo" button
    And   I select "Instagram" icon
    Then  I verify Instagram share screen appears
    When  I fill "This is Guardian galaxy blipp Photo shared" in pre-fill box of Instagram screen
    And I tap on the Instagram share button
    Then I verify that photo appears on Instagram

  Scenario: Verify the behaviour of save to device functionality
    Given I trigger the "Guardians of galaxy" blipp
    Then  I verify blipp "Guardians of galaxy" is loaded successfully
    When  I tap on the "Save to Device" button
    Then I verify that same photo of "Galaxy" blipp is saved to device gallery

  Scenario Outline: Share the Bespoke blipp photo via Facebook and Twitter
    Given I trigger the "Guardians of galaxy" blipp
    Then  I verify blipp "Guardians of galaxy" is loaded successfully
    When I tap on the "<share>" button
    Then I should see all the icons Facebook, Twitter and Email
    When I select "<social_icon>" icon
    Then I verify "<social_icon> " share pop up appears
    When I fill "<shared_text>" in pre-fill box of "<social_icon>" share pop up
    And    I tap on Post button of "<social_icon>" share pop up
    Then   I verify photo is shared on "<social_icon>"

    Examples:
      | Scenario                                                          | social_icon | shared_text                                    | share       |
      | Bespoke photo sharing on Facebook with shared text  on Facebook   | Facebook    | This is Guardians of galaxy blipp photo shared | Share Photo |
      | Bespoke photo sharing on Facebook without shared text on Facebook | Facebook    |                                                | Share Photo |
      | Bespoke photo sharing on Facebook with shared text on Twitter     | Twitter     | This is Guardians of galaxy blipp photo shared | Share Photo |
      | Bespoke photo sharing on Facebook without shared text on Twitter  | Twitter     |                                                | Share Photo |

  Scenario Outline: Verify Sharing of Bespoke blipp and Photo via Email
    Given I trigger the "Guardians of galaxy" blipp
    Then  I verify blipp "Guardians of galaxy" is loaded successfully
    When  I tap on the "<shared_content>" button
    Then  I should see all the icons Facebook, Twitter and Email
    When  I select "Email" icon
    And   I type in "qablippar@gmail.com"
    And   I tap on send button
    Then  I verify email is recieved

    Examples:
      | Scenario                        | shared_content |
      | Bespoke blipp sharing via email | Share Blipp    |
      | Bespoke photo sharing via email | Share Photo    |
