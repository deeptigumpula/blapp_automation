require_relative 'server_launcher'
require_relative 'setup'

desc 'Running Android tests!'
task :android, :type,:tag do |t, args|
  task_setup android_app,t.to_s,args
end

desc 'Running iOS tests!'
task :ios, :type, :tag do |t, args|
  app = ios_app
  task_setup app, t.to_s, args
end

def task_setup app, platform, args
  types = ['single', 'dist', 'parallel', 'translation']
  unless types.include? args[:type]
    puts "Invalid run type!\nChoose: #{types}"
    abort
  end
  setup_output_dir

  if args[:tag].nil?
    else
      if args[:tag].starts_with('@')
        tag = "-t #{args[:tag]}"
      else
        tag = "#{args[:tag]}"
      end
  end

    ENV["PROJECT_DIR"] = "#{Dir.pwd}"
    ENV["APP_PATH"] = app
    if args[:type] == "single"
      start_single_appium platform
    elsif ['dist', 'parallel', 'translation'].include? args[:type]
      launch_hub_and_nodes platform
      threads = "-n #{ENV["THREADS"]}"
      ENV["SERVER_URL"] = "http://localhost:4444/wd/hub" #Change this to your hub url if different.
  end

  case args[:type]
    when "single"
     "cucumber #{tag} -p report"
     exec "cucumber #{tag} -p report"
    when "dist"
      exec "parallel_cucumber #{threads} #{tag} -p report"
    when "parallel"
      exec "parallel_test #{threads} -e 'cucumber #{tag} -p report'"
    when "translation"
      exec "cucumber -t #{tag} -p report"
  end
end

def android_app
  "#{Dir.pwd}/build/build.apk"
end

def setup_output_dir
  system "mkdir output >> /dev/null 2>&1"
  `/bin/rm -rf ./output/allure/* >> /dev/null 2>&1`
  `rm ./output/*  >> /dev/null 2>&1`
end