require 'json'
require 'bundler'
require 'rspec'
require 'allure-cucumber'
require 'selenium-webdriver'
require 'appium_lib'
require 'parallel_tests'
require 'parallel'
require_relative 'helpers'
include AllureCucumber::DSL
Bundler.require(:test)

def thread
  ((ENV['TEST_ENV_NUMBER'].nil? || ENV['TEST_ENV_NUMBER'].empty?) ? 1 : ENV['TEST_ENV_NUMBER']).to_i
end

def get_device_data
  begin
    JSON.parse(ENV["DEVICES"]).find { |t| t["thread"].eql? thread }
  rescue
    {}
  end
end

def set_udid_environment_variable
  ENV["UDID"] = get_device_data["udid"]
end

def initialize_appium
  puts "intilize appium started"
  sleep 3
  device = get_device_data
  caps = Appium.load_appium_txt file: "#{Dir.pwd}/platforms/support/appium.txt", verbose: true
  caps[:caps][:udid] = device.fetch("udid", nil)
  caps[:caps][:deviceName] = device.fetch("name", caps[:caps][:deviceName])
  caps[:caps][:app] = ENV["APP_PATH"]
  caps[:caps][:noReset] = true
  caps[:appium_lib][:server_url] = ENV["SERVER_URL"]
  @driver = Appium::Driver.new(caps).start_driver
  puts "Appium driver is started"
  Appium.promote_appium_methods Object
  Appium.promote_singleton_appium_methods Helpers
  $device_id = device.fetch("udid", nil)
  puts $device_id
end

def quit_appium
  driver_quit
end


def allure_report_setup
  AllureCucumber.configure do |config|
    config.include AllureRubyAdaptorApi
    config.output_dir = "#{Dir.pwd}/output/allure/#{thread}/"
    config.clean_dir = true
  end

  if ParallelTests.first_process? then
    FileUtils.rm_rf(AllureCucumber::Config.output_dir)
  else
    sleep(1)
  end
end

def attach_report_files example
  example.attach_file("Hub Log: #{ENV["UDID"]}", File.new("#{Dir.pwd}/output/hub.log")) unless ENV["THREADS"].nil?
  dir = Dir.entries("#{Dir.pwd}/output/").grep(/#{ENV["UDID"]}/)
  @driver.screenshot "#{ENV["BASE_DIR"]}/output/screenshot-#{ENV["UDID"]}.png"
  files = dir.map { |file| { name: file.match(/(.*)-/)[1], file: "#{Dir.pwd}/output/#{file}" } }
  files.each { |file| example.attach_file(file[:name], File.new(file[:file])) } unless files.empty?
end