ENV['SIKULI_MIN_SIMILARITY'] ||= '0.6'

require_relative '../platforms/support/helpers/test_helpers/tool_box'

module SikuliExtension
  SIKULI_APP  = '/Applications/SikuliX.app/run'
  SCRIPT_PATH = "#{Dir.pwd}/thirdparties"

  module Base
    include ToolBox

    private_class_method *instance_methods

    def sikuli_run_script(script_name, args = '')
      script = "#{SCRIPT_PATH}/#{script_name}.sikuli"
      fail("[SIKULI] The Sikuli file #{script_name} not found in path: #{SCRIPT_PATH}") unless File.exist?(script)

      java_18_pattern = "/Library/Java/JavaVirtualMachines/jdk1.8*/Contents/Home"
      java_18_glob = Dir.glob(java_18_pattern)
      fail("[SIKULI] Java 1.8 not found in glob: #{java_18_pattern}") if java_18_glob.empty?

      run_script_with_timeout("#{args} AWT_TOOLKIT=CToolkit JAVA_HOME=#{java_18_glob.first} #{SIKULI_APP} -r #{script}")
    end

    def run_script_with_timeout(script, default_timeout = 90)
      handler_on_timeout = ->() { kill_sikuli }
      SafeThread.run_command(timeout: default_timeout,
                             max_attempts: 1,
                             error_on_timeout: '[SIKULI] Exception: Timeout per script',
                             block_on_timeout: handler_on_timeout) { Shell.execute_command(script) }
    end

    def kill_sikuli
      process_name_key = 'SikuliX.app'
      result = `ps aux | grep -v grep | grep #{process_name_key} | awk '{print $2}'`
      pids = result.split("\n")
      pids.each do |pid|
        puts "pid: #{pid} will be terminated."
        Shell.execute_command("kill -TERM #{pid}")
        sleep(0.5)
      end
    end
  end

end
