# **Blippar App Automation BDD framework** 

The tools and libraries i have used here aiming 100% automation are as follows


*1.Appium (for functional testing)
*2.OpenCv library (for blipp testing)
*3.Apis (for third party tools automation)

#Test management tool 
Hiptest (which is integrated with Automation project )

###Install Appium Dependencies
* Follow instruction [here](https://bitbucket.org/deepthigumpula/blapp_automation/src/a3bce0da09173d9a86c359109f5f3ed1863f5f97/Appium%20Mac%20Installation%20Instructions.md) for Windows.
* Follow instruction [here](https://bitbucket.org/deepthigumpula/blapp_automation/src/a3bce0da09173d9a86c359109f5f3ed1863f5f97/Appium%20Windows%20Installation%20Instructions.md) for mac.

###Clone or download this repo.
* Unzip the repo if you downloaded it.
* Move the project to `C:\Users\**your_user_name**\Blapp_automation for windows.
* Move the project to `~/Blapp_automation` for mac.

###Install Ruby Gems Dependencies
* Open a terminal or shell window.
* Goto `cd Blapp_automation`
* Install bundler `gem install bundler`
* Install appium_lib `gem install appium_lib`
* Install appium_console `gem install appium_console -v 2.0.1`
* Install rspec `gem install rspec`
* Install os `gem install os`
* Install Imagemagick and then install gem rmagick
* Install Opencv gem
https://github.com/ruby-opencv/ruby-opencv
or 
brew install homebrew/science/opencv
* Install `gem install bundler`
* Add "Bundle install" to command line to install all gems in the Gem file

###Run the sample test
* Double click the Appium icon on windows desktop or run from applications folder for mac.
* Click the Play/Start button.
* Start your EM1 android emulator you created in the setup or connect any device 
* Open CMD or terminal and goto `cd C:\Users\your_user_name\blapp_automation\` or `cd ~/blapp_automation`.
* Then run `Rake android[single]` if you wish to run tests on single device and 'Rake android[multiple] if you wish to run tests on multiple devices.
* Yay!! it should start running 


###Framework Explanation

* Tool used :Appium 
* Framework : BDD(Cucumber)
* Design Pattern : PageObject 
* Folder structure :Features ,Stepdefinitions ,support folder and cucumber.yml file 
* Server launcher file ,set up file helps in launching the server pragmatically and helps to run on any device without hardcoding device id 
* test data folder :Has the list of blipp ids 
* Third party folders : list the thirdparty tool scripts
* Output folder :List the logs and screenshots 
* Rake file for configuration of rake tasks

###For Opencv integration

* Opencv gem
https://github.com/ruby-opencv/ruby-opencv


##To Run : 
*Add "Rake android[single]" to command line to run on single device. 

*Add "Rake android[multiple]" to command line to run on multiple devices.

*Add "Rake android[single,tag] to command line to run w.r.t tag on the devices.

*Add "Rake android[single,file path] to command line to run w.r.t folder or line number or feature file path" on devices.

Note :The above setup is only for android right now.Ios will be updated soon.