require_relative '../../../platforms/android/modules/social_module_android'
class BlippPhotoPageAndroid < Appium::MWBase
  include CommonSocialPanel

  @@instagram_icon = {id: 'blipp_button_Instagram'}
  @@save_to_device_icon = {id: 'blipp_button_saveToDevice'}
  @@facebook_post_button = {id: 'com.facebook.katana:id/post_button'}
  @@facebook_status = {id: 'com.facebook.katana:id/status_text'}
  @@instagram_crop_view = {id: 'com.instagram.android:id/crop_image_view'}
  @@save_insta_button = {id: 'com.instagram.android:id/save'}
  @@share_insta_button = {id: 'com.instagram.android:id/next_button_textview'}
  @@next_insta_button = { id:'com.instagram.android:id/next_button_textview'}

  def trait
    @@save_to_device_icon
  end

  def verify_social_share_icons
    check_elements_exist([@@facebook_icon, @@twitter_icon, @@email_icon, @@instagram_icon])
  end

  def tap_on_post_button(social_icon)
    case social_icon
      when "Facebook"
        touch(@@facebook_post_button)
      when "Twitter"
        touch(@@twitter_post_button)
    end
  end

  def tap_on_save_to_device_button
    touch(@@save_to_device_icon)
  end

  def tap_on_instagram_icon
    touch(@@instagram_icon)
  end

  def verify_photo_posted_on_instagram(blipp_name)
    Assertions.assert_equal(Instagram::API.instagram_photo_caption(blipp_name).to_s,blipp_name.to_s,'Blipp photo is not  saved to instagram')
  end

  def verify_instagram_share_screen_appears
    check_element_exists(@@instagram_crop_view)
    wait_for_element_exists(@@save_insta_button)
    touch(@@save_insta_button)
    wait_for_element_exists(@@next_insta_button)
    touch(@@next_insta_button)
  end

  def fill_in_text_in_instagram_pop_up(blipp_name)
    wait_for_element_exists(@@share_insta_button)
    find_element(id:'com.instagram.android:id/caption_text_view').click
    find_element(id:'com.instagram.android:id/caption_text_view').clear
    find_element(id:'com.instagram.android:id/caption_text_view').send_keys(blipp_name)
  end

  def tap_on_insta_share_button
    touch(@@share_insta_button)
  end
end