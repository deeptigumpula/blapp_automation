require_relative '../../../platforms/support/facebook'
require_relative '../../../platforms/support/Instagram'
require_relative '../../../platforms/android/modules/social_module_android'

class BlippPageAndroid < Appium::MWBase
  include Facebook::API
  include Instagram::API
  include CommonSocialPanel

  @@share_button = {id: 'blipp_button_share'}
  @@favourites_button = {id: 'blipp_button_save'}
  @@camera_icon = {id: 'blipp_button_photo'}
  @@facebook_share_button = {id: 'com.facebook.katana:id/button_share'}
  @@tweet_text = {id: 'com.twitter.android:id/tweet_text'}
  @@tweet = "I've just blipped this. Download the Blippar app and blipp this: https://blippar.com/en/share/m51764"
  @@save_to_device_icon = {id: 'blipp_button_saveToDevice'}
  @@twitter_post_button = {id: 'com.twitter.android:id/composer_post'}


  def trait
    @@camera_icon
  end

  def tap_on_share_button
    wait_for_element_exists(@@share_button)
    touch(@@share_button)
    puts "tapped on share icon"
    wait_for_element_exists(@@facebook_icon)
  end

  def tap_share_photo_icon
    touch(@@camera_icon)
    wait_for_element_exists(@@save_to_device_icon)
  end

  def verify_social_share_icons
    check_elements_exist([@@facebook_icon, @@twitter_icon, @@email_icon])
  end

  def tap_on_post_button(social_icon)
    case social_icon
      when "Facebook"
        touch(@@facebook_share_button)
      when "Twitter"
        touch(@@twitter_post_button)
    end
  end

end