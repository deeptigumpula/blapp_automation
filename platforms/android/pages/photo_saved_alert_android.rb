class PhotoSavedAlertPage < Appium::MWBase
  @@alert_title = {xpath:'//android.widget.TextView[@text="Photo saved!"]'}
  @@dismiss_button = { id:'Common_dismissbutton'}

  def trait
   @@alert_title
  end

  def verify_photo_saved_alert
    check_element_exists(@@dismiss_button)
    touch(@@dismiss_button)
  end

end