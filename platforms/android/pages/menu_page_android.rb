class MenuPageAndroid < Appium::MWBase
  @@next_button = "textview_button_onboarding_next_label"
  @@help_with_blipping = {xpath: '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[2]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.cg[4]'}
  @@settings_menu_option = {xpath: '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[2]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.bb[6]'}

  @@menu_button = {id: 'loading_spinner_container'}
  @@settings_reference = {id: 'textView_broadmatch_mode_toggle_label'}

  def trait
    @@settings_menu_option
  end

  def tap_help_with_blipping
    wait_for_element_exists(@@help_with_blipping)
    touch(@@help_with_blipping)
  end

  def tap_on_next_button
    selectById(@@next_button)
  end

  def get_all_help_for_blipping_screenshots(screen_name,lang)
    i = 1
    while i < 5 do
      tap_on_next_button
      get_screenshot("#{screen_name}_#{i}_#{lang}")
      puts "Printed screen:#{screen_name}_#{i} in language #{lang}"
      i+=1
    end
  end


  def tap_on_menu_button
    touch(@@menu_button)
  end

  def go_to_settings
    touch(@@settings_menu_option)
    wait_for_element_exists(@@settings_reference)
  end
end
