class SettingsPage< Appium::MWBase

  @@enter_code = {xpath: '//*[@text = "Enter a code"]'}
  @@edit_code_box = {id: 'enter_code_editBox'}
  @@more_text = {xpath: '//android.widget.TextView[@text="More"]'}
  @@confirm_button = {id: 'confirm_button_centre'}
  @@change_code = {xpath: '//*[@text = "Change code"]'}
  @@confirm_button2 = {id: 'confirm_button_right'}

  def trait
    @@more_text
  end

  def enter_blipp_code(blipp_name)
    (@enter_code_text.eql? "Enter a code") ? touch(@@enter_code) : touch(@@change_code)
    wait_for_alert(2)
    fill_in_code(blipp_name)
  end

  def fill_in_code(blipp_name)
    address = blipp_address(blipp_name)
    code = "address:#{address}"
    web_element(@@edit_code_box).click
    web_element(@@edit_code_box).clear
    web_element(@@edit_code_box).send_keys(code)
    element_exists(@@confirm_button) ? touch(@@confirm_button) : touch(@@confirm_button2)
  end

  def scroll_to_enter_code(text)
    @enter_code_text = text
    scroll_to_exact_text(@enter_code_text)
  end

  def tap_on_change_code
    wait_for_element_exists(@@change_code)
    touch(@@change_code)
    wait_for_element_exists(@@enter_code)
  end

end