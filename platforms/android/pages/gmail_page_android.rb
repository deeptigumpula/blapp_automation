class GmailPage < Appium::MWBase

  @@compose_text = {xpath: '//*[@text = "Compose"]'}
  @@send_button = {xpath:'//*[@content-desc= "Send"]'}
  @@subject = {id:'com.google.android.gm:id/subject'}
  @@body={xpath:'//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.FrameLayout[2]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[1]'}

  def trait
    sleep 3
    @@compose_text
  end

  def tap_on_send_button
    touch(@@send_button)
  end

  def type_in_email(social_option)
    find_element(id:'com.google.android.gm:id/to').click
    find_element(id:'com.google.android.gm:id/to').send_keys social_option
     check_content
  end

  def check_content
    check_element_exists(@@body)
    shared_content = find_element(@@body).attribute("contentDescription")
    text = shared_content.gsub(/\r?\n/, " ").rstrip.split.join(" ")
    return text
  end

  def type_in_subject(blipp_name)
    subject = find_element(id:'com.google.android.gm:id/subject')
    wait_for_element_exists(@@subject)
    subject.click
    text = "The #{blipp_name} is shared"
    subject.send_keys text
  end

  def verify_email_recieved(email_content)
    Assertions.assert_includes(GmailAuth::API.get_email_details, email_content,"Email not recieved")
  end
end
