class ScanningPageAndroid < Appium::MWBase
  @@torch_icon = {id: 'iv_home_toggle_flash'}
  @@camera_icon = {id: 'iv_home_toggle_camera'}
  @@history_tab = {id: 'tab_nav_history'}
  @@favourites_tab = {id: 'tab_nav_favorites'}
  @@scanning_tab = {id: 'tab_nav_scanning'}
  @@faceprofile_tab = {id: 'tab_nav_profile'}
  @@settings_tab = {id: 'tab_nav_menu'}
  @@code_tab = {id: 'code_tab'}

  def trait
    @@torch_icon
  end

  def code_tab_exists
    element_exists(@@code_tab)
  end
  def verify_default_screen
    check_elements_exist([@@camera_icon, @@torch_icon, @@history_tab, @@favourites_tab, @@scanning_tab, @@faceprofile_tab, @@settings_tab])
  end

  def launch_settings_screen
    touch(@@settings_tab)
    page(SettingsPage).await
  end

  def tap_blipp_share_button
    wait_for_element_exists(@@blipp_share_button)
    touch(@@blipp_share_button)
  end

  def tap_on_post_button
    id(@@share_button).click
  end

  def find_facebook_icon
    if exists { id('android:id/tw_resolver_page_navi') } && image_exists('facebook.png')
      touch_image_screen('facebook.png')
    else
      system("adb -s #{$device_id} shell input swipe 561 1659 573 745")
      touch_or_image_screen('facebook1.png')
    end
  end

  def tap_on_facebook_icon
    sleep 2
    puts "tapping on facebook icon"
    find_facebook_icon
  end

end