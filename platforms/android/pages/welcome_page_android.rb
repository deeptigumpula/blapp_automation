class WelcomePage < Appium::MWBase
  @@title = {xpath: '//*[@text = "Welcome!"]'}
  @@start_blipping_button ={xpath: '//*[@text = "start blipping"]'}

  def trait
    @@title
  end

  def tap_on_start_blipping_button
    check_element_exists(@@start_blipping_button)
    touch(@@start_blipping_button)
  end


end