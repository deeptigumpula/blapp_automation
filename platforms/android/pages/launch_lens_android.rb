class LensSelectorPage < Appium::MWBase
  @@lens_selector = {id: 'bt_home_lens_selector'}
  @@lens_view = {id:'tv_name'}

  def trait
    @@lens_view
  end

  def tap_on_discovery_lens
    touch(@@lens_view)
  end
end

