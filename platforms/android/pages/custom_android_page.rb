class AlertPage < Appium::MWBase
  @@alert_pop_up = {id: 'alertTitle'}
  @@button_started = {id: 'button_get_started'}
  @@allow_button = {id: 'permission_allow_button'}
  @@permission_button = {xpath: '//android.widget.Button[2]'}

  def skip_promo_update
    puts "test checking alerts"
    skip_location_update
    skip_build_update
  end

  def allow_data_access
    touch(@@permission_button) if element_exists(@@permission_button)
    sleep 1
    touch(@@permission_button) if element_exists(@@permission_button)
  end

  def skip_location_update
    if element_exists(@@button_started)
      touch(@@button_started)
    end
    allow_data_access
  end

  def skip_build_update
    if element_exists(@@alert_pop_up)
      if text_of_element(@@alert_pop_up).equal? 'Update Available'
        selectByXpath('//android.widget.Button[1]')
        puts "tapped on update button"
      end
    end
  end

end