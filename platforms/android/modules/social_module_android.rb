module CommonSocialPanel
  @@facebook_icon = {id: 'blipp_button_facebook'}
  @@twitter_icon = {id: 'blipp_button_tweet'}
  @@email_icon = {id: 'blipp_button_email'}
  @@cross_button = {id: 'crossbutton'}
  @@facebook_pop_up_view = {id: 'com.facebook.katana:id/title_container_view'}
  @@gmail_icon = {xpath:'//*[@text = "Gmail"]'}
  @@social_pop_up = {xpath:'//*[@text = "Choose an email client"]'}
  @@twitter_post_button = { id:'com.twitter.android:id/composer_post'}
  @@facebook_status = {id: 'com.facebook.katana:id/status_text'}
  @@tweet_text = {id: 'com.twitter.android:id/tweet_text'}

  def tap_on_facebook_icon
    touch(@@facebook_icon)
  end

  def tap_on_twitter_icon
    touch(@@twitter_icon)
  end

  def tap_on_email_icon
    touch(@@email_icon)
    wait_for_element_exists(@@social_pop_up)
    tap_on_gmail_icon
  end

  def tap_on_gmail_icon
    touch(@@gmail_icon)
  end

  def enter_status_text(text,social_icon)
    case social_icon
      when "Facebook"
        touch(@@facebook_status)
        find_element(:id, 'com.facebook.katana:id/status_text').send_keys text
      when "Twitter"
        touch(@@tweet_text)
        find_element(id:'com.twitter.android:id/tweet_text').clear
        find_element(id:'com.twitter.android:id/tweet_text').send_keys text
    end

  end

  def share_pop_up_exists(social_option)
    case social_option
      when "Facebook"
        wait_for_element_exists(@@facebook_pop_up_view)
      when "Twitter"
        wait_for_element_exists(@@tweet_text)
    end

  end


  def verify_blipp_shared_on_facebook(status, time)
    #t = true if Facebook::API.get_status_time.include? time
    ##Assertions.assert_true(t ,'Please check this might be a old post.and latest one is not posted on facebook')
    Assertions.assert_equal(Facebook::API.get_blipp_status_on_facebook.to_s, status.to_s, 'Facebook status of the blipp shared is not same.This blipp is not the same blipp posted')
  end

  def verify_message_on_twitter(status)
    Assertions.assert_includes(Twitter::API.get_twitter_blipp_status.to_s, status.to_s, 'Blipp is shared on twitter and status is checked')
  end
end