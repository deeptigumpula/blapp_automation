

def android_setting_set(name, value, context = 'system')
  system(adb_command +
             " shell content insert --uri content://settings/#{context} --bind name:s:#{name} --bind value:#{value}")
end

def android_setting_query(name, value)
  (`#{adb_command} shell content query --uri content://settings/system --where "name = '#{name}'"`).include? value
end

def disable_device_auto_rotate
  # Ensure phone is in portrait (or landscape for tablet)
  if TABLET
    # turn auto rotate on
    android_setting_set('accelerometer_rotation', 'i:1')
    # rotate to portrait
    # android_setting_set('user_rotation', 'i:0')
    # rotate to landscape
    android_setting_set('user_rotation', 'i:1')
    # turn auto rotate off
    android_setting_set('accelerometer_rotation', 'i:0')
    # rotate to landscape
    # android_setting_set('user_rotation', 'i:1')
  else
    android_setting_set('accelerometer_rotation', 'i:0') if android_setting_query('accelerometer_rotation', 'value=1')
    android_setting_set('user_rotation', 'i:0') if android_setting_query('user_rotation', 'value=1')
  end
end

def emulator?
  !%x( #{adb_command} shell getprop | grep qemu ).empty?
end

def wifi_connected?
  return true if emulator?
  # wifi connected state is reported in different way depending on device
  wifi_current_state_one_of?({ most1: 'ConnectedState',
                               most2: 'OnlineState',
                               most3: 'CompletedState',
                               most4: 'DeviceActiveState',
                               some: 'LinkMonitoringState' }.values)
end

def wifi_disconnected?
  return true if emulator?
  # wifi disconnected state is reported in different way depending on device
  wifi_current_state_one_of?({ most: 'NotConnectedState',
                               htc: 'UninitializedState' }.values)
end

def wifi_current_state_one_of?(*state)
  `#{adb_command} shell dumpsys wifi | grep -e '^curState=\\(#{state.join('\\|')}\\)\r$' >/dev/null 2>&1 ; echo $?`.strip.to_i.zero?
end

def enable_airplane_mode
  android_setting_set('airplane_mode_on', 'i:1', 'global')
  system(adb_command + ' shell am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true')
  wait_for(timeout: 60,
           timeout_message: 'Wifi did not disconnect after enabling airplane mode') do
    wifi_disconnected?
  end
end

def disable_airplane_mode
  android_setting_set('airplane_mode_on', 'i:0', 'global')
  system(adb_command + ' shell am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false')

  # android_setting_set('wifi_on', 'i:0', 'global')
  # system(adb_command + ' shell am broadcast -a android.net.wifi.WIFI_STATE_CHANGED')
  #
  # android_setting_set('wifi_on', 'i:1', 'global')
  # system(adb_command + ' shell am broadcast -a android.net.wifi.WIFI_STATE_CHANGED')

  wait_for(timeout: 60,
           timeout_message: 'Wifi did not connect after disabling airplane mode') do
    wifi_connected?
  end

  # BDGuest can't resolve .d3 and similar.
  wifi_network = %x(#{adb_command} shell dumpsys netstats | grep -E 'iface=wlan.*networkId' | cut -d '"' -f2 | head -1)
  if wifi_network.casecmp('BD')
    warn "WIFI ssid is #{wifi_network}"
  else
    warn "**\n** WARNING: WIFI ssid is #{wifi_network}\n**\n"
  end
end

def adb_command
  if ENV['ADB_DEVICE_ARG']
    "adb -s #{ENV['ADB_DEVICE_ARG']}"
  else
    'adb'
  end
end

def press_back_button
  system(adb_command + ' shell input keyevent KEYCODE_BACK')
end

def return_sdk_level
  `#{adb_command} shell grep ro.build.version.sdk= system/build.prop`.split('sdk=').last.to_i
end

def device_model
  `#{adb_command} shell getprop ro.product.model`.strip
end

def connected_devices_battery_info
  puts ''
  puts '---- Battery debug info ----'
  puts ''
  connected_devices.each do |device_serial|
    puts "device: #{device_serial}, battery #{`adb -s #{device_serial} shell dumpsys battery | grep level`.strip} %"
  end
  puts ''
end

def connected_devices
  output = `adb devices`
  devices = output.split("\n").map(&:strip)
  devices.map do |device|
    words = device.split(/\s/).map(&:strip)
    words.first if words.last == 'device' # return only device_id from lines that looks like «064228da2526d5b1	device»
  end.compact
end


def attach_device_logs(mem)
  prefix = (ENV['SCREENSHOT_PATH'] || '').gsub(':', '_')

  file_path = "#{Dir.pwd}/logs/android/#{prefix}android_device_logs_#{Time.now.strftime('%H-%M-%S')}_#{mem}"

  # cmd = "#{adb_command} logcat -d -v time | grep `#{adb_command} shell ps | grep #{package_name(ENV['APP_PATH'])} | cut -c10-15`"
  cmd = "#{adb_command} logcat -d -v time"
  log_outout = `#{cmd}`
  File.write(file_path, log_outout)
  embed(file_path, 'text/plain', file_path)
end

def get_memory_usage_of_app
 puts system("adb shell dumpsys meminfo 'com.blippar.ar.android.betaForEmulator'")
end

def get_cpu_information
  puts system("adb shell dumpsys cpuinfo")
end

def tap_on_done_button
  "adb -s #{device_id} shell input keyevent 66"
end