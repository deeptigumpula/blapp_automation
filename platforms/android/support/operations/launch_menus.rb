require_relative '../../../support/mwebbase'
require_relative '../../../../platforms/support/test_helper_mweb'
require_relative '../../../support/json'


def launch_scanning_screen
  skip_promo_views
  if element_exists({id: 'bt_home_lens_selector'})
    touch({id: 'bt_home_lens_selector'})
    @page = page(LensSelectorPage).await
    page(LensSelectorPage).tap_on_discovery_lens
  else
    check_element_exists({id: 'bb_bottom_bar_icon'})
  end
  @page = page(ScanningPageAndroid).await
end

def skip_promo_views
  if element_exists({id: 'alertTitle'})
    dismiss_alert if text_of_element({id: 'alertTitle'}) == 'Update Available'
  end
  if page(WelcomePage).current_page?
    @page = page(WelcomePage).await
    @page.tap_on_start_blipping_button
    wait_for_elements_do_not_exist([{id: 'iv_logo'}])
  end
end

def check_blipp_loaded(blipp_name)
  address = blipp_address(blipp_name)
  text_file ="debug_log_#{$device_id}.text"
  system("adb -s #{$device_id} logcat -d | grep 'BlippAppSupport' > #{text_file}")
  check = true if (File.readlines("#{text_file}").grep(/address: #{address}/).size > 0) && (File.readlines("#{text_file}").grep(/Blipp Loading with/).size > 0)
  FileUtils.rm_f("#{text_file}")
  return check
end

def launch_gallery_app
  `adb -s #{$device_id} shell am start  -t image/* -a android.intent.action.VIEW`
  puts "launched gallery app"
  sleep 3
end