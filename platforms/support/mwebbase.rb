require 'selenium-webdriver'
require 'test/unit/assertions'
require 'rubygems'
require 'appium_lib'
require 'page-object'
require 'cucumber'
require_relative '../../setup'
require_relative '../../platforms/support/failure_helper_mweb'
require_relative '../../thirdparties/sikuli_extension'
require_relative '../../platforms/support/test_helper_mweb'
require_relative '../support/blippdata/test_ocr'
require_relative '../support/json'
require_relative '../support/facebook'
require_relative '../support/twitter_api'
require_relative '../support/Instagram'
require_relative '../support/gmail1'

module Appium
  class MWBase
    include Appium::Cucumber::FailureHelpers
    include Appium::MobileWeb::TestHelpers
    include Test::Unit::Assertions
    include SikuliExtension::Base
    include OCR::ImageText
    include Facebook::API
    include Twitter::API
    include Instagram::API
    include GmailAuth::API
    attr_accessor :world, :transition_duration


    def back_button_exists?
      !web_elements(back_button).select(&:displayed?).empty?
    end

    def initialize(world, transition_duration = 0.5)
      self.world = world
      self.transition_duration = transition_duration
    end

    def trait
      fail 'You should define a trait method or a title method' unless respond_to?(:title)
      "* marked:'#{self.title}'"
    end

    def blippar
      puts "hi"
    end
    def page(clz, *args)
      clz.new(world, *args)
    end

    def embed(*args)
      world.embed(*args)
    end

    def await(wait_opts = {})
      wait_opts[:timeout_message] ||= "Timed out awaiting «#{self.class.name}»"
      wait_opts[:timeout] ||= 10
      poll_for(wait_opts) do
        self.current_page?
      end
      sleep(1) # TODO: :'(
      self
    end

    def current_page?
      element_exists(trait)
    end

    def selectTab(tab_name)
      selectByName(tab_name)
    end

    def selectPanel(panel_name)
      selectByName(panel_name)
    end

    def selectByName(name)
      waitElement { byName(name).click }
    end

    def byName(name)
      find_element(:name, "#{name}")
    end

    def selectById(id)
      waitElement { byId(id).click }
    end

    def selectByXpath(xpath)
      waitElement { byXpath(xpath).click }
    end

    def byId(id)
      find_element(:id, "#{id}")
    end

    def byXpath(xpath)
      find_element(:xpath, "#{xpath}")
    end

    def element_selected (element)
      find_element(element).isChecked
    end
    def verifyTextNotExist(content)
      raise("Should not find text: #{content}") unless texts(content).empty?
    end

    def verifyTextExist(content)
      #sleep 5
      raise("Not find text: #{content}") if texts(content).empty?
    end


    def verifyButtonNotExist(button_name)
      raise("find button: #{button_name}") if exists { button(button_name) }
    end

    def verifyButtonExist(button_name)
      raise("Not find button: #{button_name}") unless exists { button(button_name) }
    end

    def verifyItemNotExistById(item_id)
      waitElement {
        if exists { byId(item_id) }
          raise("find button: #{item_id}")
        end }
    end

    def verifyExistById(item_id)
      waitElement { raise("Not find item: #{item_id}") unless exists { byId(item_id) } }
    end

    def verifyItemExistByXpath(item_xpath)
      waitElement { raise("Not find item: #{item_xpath}") unless exists { byXpath(item_xpath) } }
    end

    def scroll_up(x,y)

    end

    def waitElement
      timeout = 30
      polling_interval = 0.2
      time_limit = Time.now + timeout
      loop do
        begin
          yield
        rescue Exception => error
        end
        return if error.nil?
        raise error if Time.now >= time_limit
        sleep polling_interval
      end
    end

    def send_app_to_background(time)
      time=time.to_i
      system("adb shell input keyevent 3")
      sleep(time)
      puts "Sending app to background"
      system("adb shell am start -n com.blippar.ar.android.betaForEmulator/com.blippar.ar.android.activities.BlipparMainActivity")
      puts "Sending app to foregrounded"
    end

    def swipe_via_adb(x,y,x1,y1)
      system("adb -s #{$device_id} shell input swipe  #{x} #{y} #{x1} #{y1}")
    end


    def get_screenshot(screen_name)
      screenshot_embed(name: "screenshot_of_#{screen_name}")
    end

    def fe locator
      find_element locator
    end

    def fa locator
      find_elements locator
    end

    def click locator
      fe(locator).click
    end

    def find_text locator
      fe(locator).text
    end

    def is_displayed?(locator)
      begin
        fe(locator).displayed?
      rescue
        false
      end
    end

  end
end
