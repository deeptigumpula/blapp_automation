require 'rubygems'
require 'zip'
require 'zip/zip'
require 'zip/zipfilesystem'
require 'rspec/expectations'
require 'allure-cucumber'
require 'appium_lib'
require 'cucumber/ast'
require 'selenium-webdriver'
require_relative '../../platforms/support/constants'
require 'yaml'
require_relative '../../setup'
require_relative '../../platforms/support/mwebbase'
require_relative '../../server_launcher'
# Create a custom World class so we don't pollute `Object` with Appium methods

set_udid_environment_variable

Before do
  puts "start intialising"
  initialize_appium
end

def clear_data
  `adb shell pm clear com.blippar.ar.android.betaForEmulator`
end

Before do |e|
  helper.start_video_record ENV["UDID"]
  helper.start_logcat ENV["UDID"]
end

After do |e|
  helper.stop_video_record ENV["UDID"]
  helper.stop_logcat
  screenshot "#{PROJECT_ROOT}/output/screenshot-#{ENV["UDID"]}.png"
  attach_report_files e
  puts "quiting driver"
  quit_appium
end


at_exit do
  ENV['ARCHIVE_RESULTS'] = 'no' if ENV['ARCHIVE_RESULTS'].nil?
  if ENV['ARCHIVE_RESULTS']=="yes"
    Dir.mkdir("resultsarchive") if Dir["resultsarchive"].empty?
    directoryToZip = "output"
    outputFile = "#{Date.today}_results.zip"

    zf = ZipFileGenerator.new(directoryToZip, outputFile)
    zf.write
    FileUtils.mv(outputFile, "resultsarchive/#{outputFile}")
  end
end

require 'zip/zip'

# This is a simple example which uses rubyzip to
# recursively generate a zip file from the contents of
# a specified directory. The directory itself is not
# included in the archive, rather just its contents.
#
# Usage:
#   directoryToZip = "/tmp/input"
#   outputFile = "/tmp/out.zip"
#   zf = ZipFileGenerator(directoryToZip, outputFile)
#   zf.write()
class ZipFileGenerator

  # Initialize with the directory to zip and the location of the output archive.
  def initialize(inputDir, outputFile)
    @inputDir = inputDir
    @outputFile = outputFile
  end

  # Zip the input directory.
  def write()
    entries = Dir.entries(@inputDir); entries.delete("."); entries.delete("..")
    io = Zip::ZipFile.open(@outputFile, Zip::ZipFile::CREATE);

    writeEntries(entries, "", io)
    io.close();
  end

  # A helper method to make the recursion work.
  private
  def writeEntries(entries, path, io)

    entries.each { |e|
      zipFilePath = path == "" ? e : File.join(path, e)
      diskFilePath = File.join(@inputDir, zipFilePath)
      puts "Deflating " + diskFilePath
      if File.directory?(diskFilePath)
        io.mkdir(zipFilePath)
        subdir =Dir.entries(diskFilePath); subdir.delete("."); subdir.delete("..")
        writeEntries(subdir, zipFilePath, io)
      else
        io.get_output_stream(zipFilePath) { |f| f.puts(File.open(diskFilePath, "rb").read()) }
      end
    }
  end

end

allure_report_setup
World(AllureCucumber::DSL)
World(Test::Unit::Assertions)
World(Appium::MobileWeb::TestHelpers)


