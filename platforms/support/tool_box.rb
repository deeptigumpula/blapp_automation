require 'open3'

module ToolBox
  module Shell
    class << self
      # INFO: It executes the command ignoring exceptions
      def execute_command(cmd, ignore_error_print = false)
        out, err, status = Open3.capture3(cmd)
        puts("[SHELL] Command <#{cmd}>")
        puts("[SHELL] Status: #{status.success?}")
        puts("[SHELL] Error: #{err}") unless status.success? || ignore_error_print
        out
      end
    end
  end

  module SafeThread
    class << self
      def default_options
        { timeout: 90, max_attempts: 2, error_on_timeout: 'Exception: Timeout per operation.', block_on_timeout: nil }
      end

      def run_command(options = {})
        options = default_options.merge(options)

        result = nil
        stop = false
        mutex = Mutex.new

        options[:max_attempts].times do |current_attempt|
          time_before = Time.now

          operation_thread = Thread.start do
            result = yield
            mutex.synchronize { stop = true }
          end

          await_thread = Thread.new do
            options[:timeout].times do
              break if stop
              sleep(1)
            end
          end

          await_thread.join

          time_after = Time.now
          STDOUT.puts("[UTILITY] Time per operation: #{time_after - time_before}")

          if stop
            break
          else
            STDOUT.puts("[UTILITY] #{options[:error_on_timeout]} Try to repeat operation, attempt: #{current_attempt}")
            options[:block_on_timeout].call unless options[:block_on_timeout].nil?
            operation_thread.kill if operation_thread.alive?
          end
        end

        fail("[UTILITY] #{options[:error_on_timeout]} Max attempts <#{options[:max_attempts]}> with timeout <#{options[:timeout]}> seconds.") unless stop

        result
      end
    end
  end
end
