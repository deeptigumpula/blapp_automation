# rubocop:disable Metrics/ModuleLength
require 'uri'
require 'yaml'

module Appium
  module MobileWeb
    module TestHelpers
      include Selenium::WebDriver::Error

      def wait(timeout = 5, timeout_message = nil)
        Selenium::WebDriver::Wait.new(timeout: timeout, message: timeout_message) # seconds
      end

      def driver
        $driver.driver
      end

      def wait_for_element_exists(element)
        poll_for(timeout_message: "Element not found #{element}") do
          element_exists(element)
        end
      end

      def check_element_exists(element)
        poll_for(timeout: 3, timeout_message: "Element should exist: locator #{element}") { element_exists(element) }
      end

      def check_elements_exist(elements_arr)
        elements_arr.each { |element| check_element_exists(element) }
      end

      def check_elements_do_not_exist(elements_arr)
        elements_arr.each { |element| check_element_does_not_exist(element) }
      end


      def check_view_with_mark_exists(expected_mark)
        element = {xpath: "//*[text() = '#{expected_mark}']"}
        element_exists(element)
      end

      def check_views_with_mark_exist(marks)
        marks.each { |mark| check_view_with_mark_exists(mark) }
      end

      def check_element_does_not_exist(element)
        actual = web_elements(element).find(&:displayed?)
        assert_nil(actual, "Element should not exist: locator #{element}")
      end

      def wait_for_elements_do_not_exist(elements_arr, options = {})
        if elements_arr.is_a?(String)
          elements_arr = [elements_arr]
        end
        options[:timeout_message] = options[:timeout_message] || "Timeout waiting for no elements matching: #{elements_arr.join(',')}"
        poll_for(options) do
          elements_arr.none? { |q| element_exists(q) }
        end
      end

      #This method gives the value of the attribute of that element query.eg:find_element(@class='textview')
      def get_value_of_element(query,attribute_option)
        value = find_element(query).attribute(attribute_option)
        return value
      end

      def scroll_to_text(text)
        driver.scroll_to(text)
      end

      def scroll_to_exact_text(text)
        driver.scroll_to_exact(text)
      end

      def web_element(element)
        driver.find_element(element)
      rescue NoSuchElementError, StaleElementReferenceError => e
        raise e, "Can't find element #{element}"
      end

      def web_elements(element)
        driver.find_elements(element)
      end

      def text_of_element(element)
        string = driver.find_element(element).text
        return string
      end

      def element_exists(element)
        web_element(element).displayed?
          # !web_elements(element).find(&:displayed?).nil? # How it should be
      rescue NoSuchElementError
        return false
      rescue StaleElementReferenceError
        retry
      end

      def elements_exist(elements)
        fail('Incorrect using of method: please check the incoming parameters.') unless elements.is_a?(Array)
        elements.all? { |element| element_exists(element) }
      end

      def element_does_not_exist(element)
        web_elements(element).empty?
      end

      def wait_for_alert(seconds)
        seconds.times do
          begin
            driver.switch_to.alert
            break
          rescue
            sleep(1)
          end
        end
      end

      def dismiss_alert
        driver.switch_to.alert_dismiss
      end

      def accept_alert
        driver.switch_to.alert_accept
      end

      class WaitError < RuntimeError
      end

      def wait_for(options = {}, &_block)
        warn("'wait_for' is DEPRECATED. Use 'poll_for' instead # #{caller[0]}")

        poll_for(options) { yield }
      end

      def wait_for_page_to_load
        poll_for { driver.execute_script('return document.readyState') == 'complete' }
      end

      def scroll_to_element_by_direction(element, element_to_scroll, direction = :down)
        # We cannot use driver.manage.window.size for Android
        # Due to the error: operation is unsupported on Android
        height = 100
        height = direction == :down ? -height : height

        20.times do
          break if element_exists(element)
          swipe(element_to_scroll, 0, height)
        end
      end

      def touch(element)
        web_element(element).click
      rescue ElementNotVisibleError
        raise("Element #{element} is invisible")
      rescue StaleElementReferenceError
        raise("Element #{element} does not exist for touch")
      rescue UnknownError => e
        raise("Can't tap on element #{element}.\nOriginal error: #{e.message}")
      end

      def escape_quotes(str)
        str.gsub("'", "\\\\'")
      end

      def unescape(s)
        YAML.load(%(---\n"#{s}"\n))
      end
    end
  end
end
