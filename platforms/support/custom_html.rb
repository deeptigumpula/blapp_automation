require 'cucumber/formatter/html'
class CustomHtml < Cucumber::Formatter::Html
  def embed_logs(src, label)
    @builder.span(class: 'embed') do |pre|
      pre << "<a href='./#{src}'>#{label}</a><br>&nbsp;"
    end
  end

  def embed_video(src, label)
    @vid_id ||= 0
    prefix = (ENV['SCREENSHOT_PATH'] || '').gsub(':', '_')
    id = "#{prefix}vid_#{@vid_id}"
    need_def = (@vid_id == 0)
    @vid_id += 1
    @builder.span(class: 'embed') do |pre|
      pre << %{
        <script> <!--
        function toggle_vid(id, skip) {
         if (skip) { return true; }
         img = document.getElementById(id);
         img.height = (isNaN(window.outerHeight) ? window.clientHeight : window.outerHeight)/2;
         img.style.display = (img.style.display == 'none' ? 'block' : 'none');
         return false
        }
        --> </script>
      } if need_def
      kind = src.split('.').last
      pre << %(<a href="" onclick="return toggle_vid('#{id}', false)">#{label}</a><br>&nbsp;
          <video id="#{id}" onclick="return toggle_vid('#{id}', /firefox/i.node_s4.json(navigator.userAgent))"
           style="display: none; border: 5px solid red" src="./#{src}" preload='none' controls>
          <source src="#{src}" kind="video/#{kind}"></video>)
    end
  end

  def embed_image(src, label)
    prefix = (ENV['SCREENSHOT_PATH'] || '').gsub(':', '_')
    id = "#{prefix}img_#{@img_id}"
    need_def = (@img_id == 0)
    @img_id += 1
    @builder.span(class: 'embed') do |pre|
      pre << %{
        <script> <!--
        function toggle_img(id) {
          img=document.getElementById(id);
          img.style.display = (img.style.display == 'none' ? 'block' : 'none');
          return false
        }
        --> </script>
      } if need_def
      pre << %(<a href="" onclick="return toggle_img('#{id}')">#{label}</a><br>&nbsp;
               <img id="#{id}" style="display: none" onclick="return toggle_img('#{id}')" src="./#{src}"/>)
    end
  end

  def embed(src, mime_type, label)
    if mime_type == 'text/plain'
      return embed_logs(src, label)
    end
    if mime_type == 'video/mp4'
      return embed_video(src, label)
    end
    super(src, mime_type, label)
  end

  def set_scenario_color_failed
    super
    unless @in_background
      @builder.script do
        @builder << %{
          {
            var s = document.getElementById('scenario_#{@scenario_number}');
            var e = s.parentElement.getElementsByClassName('scenario_file');
            if (e && e.length > 0) {
              e[0].style.color='white';
            }
          }
        }
      end
    end
  end

  def after_features(features)
    stacktrace_line_totals
    super
  end

  def stacktrace_line_totals
    # Add a count of appearances of stacktrace lines throughout the document, to help spot common failure causes.
    @builder << %{
      <script type="text/javascript">
    if (!document.stacktrace_line_totals_computed) {
      document.stacktrace_line_totals_computed = true;
      var nl = '\\n';
      var backtraces = document.querySelectorAll('div.backtrace pre');
      var stacklines = [];
      for(ix = 0; ix < backtraces.length; ++ix) {
        backtrace = backtraces[ix];
        bits = backtrace.innerHTML.split(nl);
        for (iy = 0; iy < bits.length; ++iy) {
          var lr = bits[iy];
          if ( stacklines[iy] === undefined ) {
            stacklines[iy] = {}
          }
          if ( ! stacklines[iy].hasOwnProperty(lr) ) {
            stacklines[iy][lr]={ total: 0 };
          }
          stacklines[iy][lr]['total']++;
        }
      }
      var last = [];
      for(ix = 0; ix < backtraces.length; ++ix) {
        var backtrace = backtraces[ix];
        var bits = backtrace.innerHTML.split(nl);
        result = '';
        for (iy = 0; iy < bits.length; ++iy) {
          var lr = bits[iy];
          var entry = stacklines[iy][lr]
          if (entry['ref'] === undefined) {
            if (last[iy] === undefined) {
              last[iy] = 0;
            }
            entry['ref'] = last[iy];
            last[iy] += 1;
            entry['next'] = last[iy];
          }
          result += bits[iy] + ' (<a href="#sr_' + iy + '_' + entry['next'] + '" name="sr_' + iy +
                    '_' + entry['ref'] + '">count</a>: ' + entry['total'] + ')' + nl;
        }
        backtrace.innerHTML = result;
      }
    }
      </script> }
  end
end
