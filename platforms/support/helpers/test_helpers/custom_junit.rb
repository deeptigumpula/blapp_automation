require 'cucumber/formatter/junit'

module Cucumber
  module Formatter
    class CustomJunit < Junit
      def after_test_case(test_case, result)
        # This method almost completely copy-pasted from original Junit reporter
        # Except one line with setting name_suffix (contains outline example)
        # Changed line set name_suffix before scenario name for making testcase names different for TeamCity

        test_case_name = NameBuilder.new(test_case)
        scenario = test_case_name.scenario_name
        scenario_designation = "#{test_case_name.name_suffix.strip} #{scenario}" # This line is changed
        output = create_output_string(test_case, scenario, result, test_case_name.row_name)
        build_testcase(result, scenario_designation, output)

        Interceptor::Pipe.unwrap! :stdout
        Interceptor::Pipe.unwrap! :stderr
      end
    end
  end
end
