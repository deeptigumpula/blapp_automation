module Os
  class << self
    def android?
      OS == ANDROID
    end

    def ios?
      OS == IOS
    end

  end
end

module Platform
  class << self
    def android?
      PLATFORM == ANDROID
    end

    def ios?
      PLATFORM == IOS
    end
  end
end
