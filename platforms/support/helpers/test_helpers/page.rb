def page_by_string(page)
  cls = "#{page} page".downcase.split(' ').collect(&:capitalize).join
  begin
    Object.const_get(cls)
  rescue NameError
    raise NameError, "Unknown class #{cls}, name created from: '#{page}'"
  end
end
