class WaitError < RuntimeError
end

def not_empty_not_nil(result)
  result && !(result.respond_to?(:empty?) && result.empty?)
end

def poll_for(timeout: 30, retry_frequency: 0.3, timeout_message: 'Timed out waiting...', # rubocop:disable Metrics/ParameterLists
             timeout_callback: nil, return_on_timeout: false,
             max_retries: nil,
             allowed_exceptions: nil, exception_callback: nil,
             &_block)
  # TODO: Rename to 'rescued_exceptions'
  allowed_exceptions ||= []
  allowed_exceptions = [allowed_exceptions] if allowed_exceptions.is_a?(Exception)

  exception_callback ||= -> {}
  timeout_callback ||= exception_callback

  finish = timeout && Time.now + timeout
  begin
    begin
      result = yield
    rescue *allowed_exceptions
      exception_callback.call
    end
    break if not_empty_not_nil(result)
    sleep(retry_frequency)
    max_retries &&= max_retries - 1
    now = Time.now
  end until (finish && finish < now) || (max_retries && max_retries <= 0)
  if return_on_timeout || not_empty_not_nil(result)
    result
  else
    timeout_callback.call
    raise(WaitError, timeout_message) # rubocop:disable Style/SignalException
  end
end

# options for poll_for apply
def poll_for_element_exists(uiquery, options = {})
  poll_for_elements_exist([uiquery], options)
end

# options for poll_for apply
def poll_for_elements_exist(elements_arr, options = {})
  if elements_arr.is_a?(String) || elements_arr.is_a?(Symbol)
    elements_arr = [elements_arr.to_s]
  end
  options[:timeout_message] ||= "Timeout waiting for elements: #{elements_arr.join(',')}"
  poll_for(options) do
    elements_arr.all? { |q| element_exists(q) }
  end
end

def poll_for_equals(expected, actual, options = {})
  poll_for(options) do
    yield
    actual.call == expected
  end
end
