require 'fileutils'
require 'cucumber/ast'
require 'cucumber/formatter/html'
require_relative '../../platforms/support/custom_html'



module Appium
  module Cucumber
    module FailureHelpers
      # Generates a screenshot of the app UI and saves to a file (prefer `screenshot_embed`).
      # Increments a global counter of screenshots and adds the count to the filename (to ensure uniqueness).
      #
      # @see #screenshot_embed
      # @param {Hash} options to control the details of where the screenshot is stored.
      # @option options {String} :prefix (ENV['SCREENSHOT_PATH']) a prefix to prepend to the filename (e.g. 'screenshots/foo-').
      #   Uses ENV['SCREENSHOT_PATH'] if nil or '' if ENV['SCREENSHOT_PATH'] is nil
      # @option options {String} :name ('screenshot') the base name and extension of the file (e.g. 'login.png')
      # @return {String} path to the generated screenshot
      # @todo deprecated the current behavior of SCREENSHOT_PATH; it is confusing


      def screenshot(options = { prefix: nil, name: nil })
        prefix = options[:prefix]
        name = options[:name]
        time = Time.new.strftime('%Y-%m-%d_%H-%M.%S')

        prefix = (prefix || ENV['SCREENSHOT_PATH'] || '').gsub(':', '_')
        if name.nil?
          name = 'screenshot'
        else
          if File.extname(name).downcase == 'logo1.png'
            name = name.split('logo1.png')[0]
          end
        end

        path = "#{Dir.pwd}/output/allure/#{thread}/#{prefix}#{name}_#{time}.png"
        begin
          $driver.driver.save_screenshot(path) unless $driver.driver.nil?
        rescue Selenium::WebDriver::Error::WebDriverError
          if Os.android?
            filename = File.basename(path)
            `#{adb_command} shell screencap -p /sdcard/#{filename}`
            `#{adb_command} pull /sdcard/#{filename} #{path} 2>&1`
          else
            warn('Can\'t take a screenshot')
          end
        rescue StandardError
          warn('Can\'t take a screenshot')
        end
        path
      end

      # Generates a screenshot of the app UI and embeds the screenshots in all active cucumber reporters (using `embed`).
      # Increments a global counter of screenshots and adds the count to the filename (to ensure uniqueness).
      #
      # @param {Hash} options to control the details of where the screenshot is stored.
      # @option options {String} :prefix (ENV['SCREENSHOT_PATH']) a prefix to prepend to the filename (e.g. 'screenshots/foo-').
      #   Uses ENV['SCREENSHOT_PATH'] if nil or '' if ENV['SCREENSHOT_PATH'] is nil
      # @option options {String} :name ('screenshot') the base name and extension of the file (e.g. 'login.png')
      # @option options {String} :label (uses filename) the label to use in the Cucumber reporters
      # @return {String} path to the generated screenshot



      def screenshot_embed(options = { prefix: nil, name: nil, label: nil })
        path = screenshot(options)
        filename = options[:label] || File.basename(path)
        embed(path, 'image/png', filename)
      end



      # Generates a screenshot of the app UI by calling screenshot_embed and raises an error.
      # Increments a global counter of screenshots and adds the count to the filename (to ensure uniqueness).
      #
      # @see #screenshot_embed
      # @param {String} msg the message to use for the raised RuntimeError.
      # @param {Hash} options to control the details of where the screenshot is stored.
      # @option options {String} :prefix (ENV['SCREENSHOT_PATH']) a prefix to prepend to the filename (e.g. 'screenshots/foo-').
      #   Uses ENV['SCREENSHOT_PATH'] if nil or '' if ENV['SCREENSHOT_PATH'] is nil
      # @option options {String} :name ('screenshot') the base name and extension of the file (e.g. 'login.png')
      # @option options {String} :label (uses filename) the label to use in the Cucumber reporters
      # @raise [RuntimeError] with `msg`
      def screenshot_and_raise(msg, options = { prefix: nil, name: nil, label: nil })
        warn "[Deprecated] The method screenshot_and_raise is deprecated because of screenshot duplication. Please, don't use this method. Screenshots are already taken in AfterHook if Test case failed."
        screenshot_embed(options)
        raise(msg)
      end
    end
  end
end
