# PLATFORMS
IOS = 'ios'
ANDROID = 'android'

ENV["BASE_DIR"] = Dir.pwd

IMAGES_PATH ||= File.expand_path(File.join(File.dirname(__FILE__), "blippdata/images"))
PROJECT_ROOT ||= File.expand_path('../../', File.dirname(__FILE__))
BlIPP_FILE ||= "#{PROJECT_ROOT}/testdata/blipps.json"
EMAIL_FILE ||="#{PROJECT_ROOT}/testdata/social_email.json"


