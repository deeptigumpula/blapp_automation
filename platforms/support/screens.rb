module AndroidPageDomain
  def page(clz)
    clz.new(self)
  end
end

World(AndroidPageDomain)