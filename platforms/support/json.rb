require 'rubygems'
require 'json'
require 'pp'
require_relative '../../platforms/support/constants'

def blipp_address(blipp_name)
  json = File.read(BlIPP_FILE)
  obj = JSON.parse(json)
  pp obj[blipp_name]
end

def get_email(social_option)
  json = File.read(BlIPP_FILE)
  obj = JSON.parse(json)
  pp obj[social_option]
end