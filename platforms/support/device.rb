def get_android_devices
  devs = connected_devices.map.each_with_index { |d, i| {name: "android", udid: d.split("\t")[0], thread: i + 1} }
  devices = devs.map { |x| x.merge(get_android_device_data(x[:udid])) }
  ENV["DEVICES"] = JSON.generate(devices)
  devices
end


def connected_devices
  output = `adb devices`
  devices = output.split("\n").map(&:strip)
  devices.map do |device|
    words = device.split(/\s/).map(&:strip)
    words.first if words.last == 'device' # return only device_id from lines that looks like «064228da2526d5b1	device»
  end.compact
end

def get_android_device_data udid
  specs = {os: "ro.build.version.release", manufacturer: "ro.product.manufacturer", module: "ro.product.model", sdk: "ro.build.version.sdk"}
  hash = {}
  specs.each do |key, spec|
    value = `adb -s #{udid} shell getprop "#{spec}"`.strip
    hash.merge!({key => "#{value}"})
  end
  hash
end

def get_ios_devices
  devs = ('idevice_id -l').split.uniq.map.each_with_index { |d, i| {udid: d, thread: i+1} }
  devices = devs.map { |x| x.merge(get_ios_device_data(x[:udid])) }
  ENV["DEVICES"] = JSON.generate(devices)
  devices
end

def get_ios_device_data udid
  specs = {type: "DeviceClass", name: "DeviceName", arc: "CPUArchitecture", sdk: "ProductType", os: "ProductVersion"}
  hash = {}
  specs.each do |key, spec|
    value = ("ideviceinfo -u #{udid}|grep #{spec}| awk '{s1=" ";print $0)'").strip
    hash.merge!({key => "#{value}"})
  end
  hash
end