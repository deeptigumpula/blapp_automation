require 'rubygems'
require 'timeout'
require 'open3'
include Process

def scenario_mem(scenario)
  return '' unless scenario.respond_to? :entries
  (scenario.entries.map(&:value)).join('.').gsub(/\W/, '')[0, 25]
end

def video_file(scenario, infix, ext = 'mp4')
  name = "video_#{infix}"
  name << "_#{scenario_mem(scenario)}" unless scenario_mem(scenario).nil? || scenario_mem(scenario).empty?
  name << ".#{ext}"
  name
end

capture_video = (ENV['CAPTURE_VIDEO'] || 'failure').downcase

class ScreenRecord
  @@set = false
  @@screenrecord = nil

  def self.cmd
    return @@screenrecord if @@set
    out = Open3.capture2e("#{adb_command} shell screenrecord --help")
    @@set = true
    @@screenrecord =
      case out
      when /--bugreport/
        'screenrecord --bugreport'
      when /not found/
        nil
      else
        'screenrecord'
      end
  end
end

class AndroidVideoCapture
  def start_capture(_embedder, scenario)
    if ScreenRecord.cmd
      @video_file = video_file(scenario, Time.now.strftime('%H%M%S'))
      @screenrecord = Open3.popen2e("#{adb_command} shell")
      @screenrecord[0] << "rm -rf /sdcard/#{video_file(nil, '*')}\n"
      @screenrecord[0] << "#{ScreenRecord.cmd} --verbose --bit-rate 100000 /sdcard/#{@video_file} &\n"
      @screenrecord[0] << "CPID=$!\n"
    end
  end

  def stop_capture(_embedder)
    @screenrecord[0] << "if ps | grep screenrecord | grep $CPID; then kill -2 $CPID ; fi\n"
    @screenrecord[0] << "while ps | grep screenrecord | grep $CPID ; do sleep 1; done; exit\n"
    begin
      Timeout.timeout(5) { @screenrecord[2].value }
    rescue Timeout::Error
      Process.detach(@screenrecord[2].pid)
    end
    File.open("#{@video_file}.log", 'w') { |f| f << @screenrecord[1].readlines.join("\n") }
    @screenrecord[0].close
    @screenrecord[1].close
  end

  def acquire_capture(embedder, _scenario)
    system "#{adb_command} pull /sdcard/#{@video_file} . 2>&1"
    if File.exist?(@video_file)
      embedder.embed(@video_file, 'video/mp4', "video #{@video_file}")
      # embed("#{@video_file}.log", 'text/plain', 'capture log')
      File.delete("#{@video_file}.log")
    end
  end

  def dispose_capture(_embedder)
    File.delete("#{@video_file}.log")
  end
end

class MacQuickTimeCapture
  # http://www.paulcalnan.com/archives/2014/10/quicktime-screen-recording-of-a-single-window.html
  CAPTURE_DIR = "#{PROJECT_ROOT}/features/support/hooks/hooks_video_ios"
  CAPTURE_SCRIPT = "#{CAPTURE_DIR}/videocapture.applescript"

  # iOS can't start capture until the simulator's running, which is delayed unless no_deeplink.
  def initialize(skip_start_count)
    @skip_start_count = skip_start_count
  end

  def start_capture(_embedder, _scenario)
    if @skip_start_count == 0
      %x( killall 'QuickTime Player' ) # Workaround Apple flakiness
      %x( #{CAPTURE_SCRIPT} start Simulator 1 #{CAPTURE_DIR}/clickdrag )
    end
    @skip_start_count -= 1
  end

  def stop_capture(_embedder)
  end

  def acquire_capture(embedder, scenario)
    sleep(3) # sleep extra 3 seconds for capturing more video after assertion thrown

    @video_base = video_file(scenario, Time.now.strftime('%H%M%S'), '')
    # Sandboxing requires saving to somewhere official first. As if someone couldn't move it afterwards. Duh.
    qtpxcomp = "#{ENV['HOME']}/Movies/#{@video_base}qtpxcomposition"
    complete = false
    Thread.new do
      sleep 10
      %x( killall 'QuickTime Player' ) unless complete # workaround apple flakiness
    end
    %x( #{CAPTURE_SCRIPT} save #{qtpxcomp} )
    complete = true
    qtpxmovie = "#{qtpxcomp}/Screen Recording.mov"
    if File.exist?(qtpxmovie)
      FileUtils.mv(qtpxmovie, "#{@video_base}mov")
      embedder.embed("#{@video_base}mov", 'video/mp4', "Movie #{@video_base}mov")
    end
  end

  def dispose_capture(_embedder)
    %x( #{CAPTURE_SCRIPT} stop )
  end
end

Before do |scenario|
  @video_capture = nil
  if capture_video != 'never'
    @video_capture = case
                       when OS == ANDROID
                         AndroidVideoCapture.new
                       when OS == IOS && ENV['IOS_VIDEO_CAPTURE'] == 'quicktime'
                         MacQuickTimeCapture.new
                       # else
                       #   nil # Pending TightVNC patches.
                     end
    @video_capture.start_capture(self, scenario) if @video_capture
  end
end

After do |scenario|
  unless @video_capture.nil?
    @video_capture.stop_capture(self)
    if scenario.failed? || scenario.source_tag_names.include?('@capture_video') || capture_video == 'always'
      @video_capture.acquire_capture(self, scenario)
    else
      @video_capture.dispose_capture(self)
    end
  end
end
