require 'rtesseract'
require_relative '../../../platforms/support/constants'
module OCR
  module ImageText
    def extract_text_from_word(string, image)
      results = []
      t = RTesseract::Box.new("#{image}").words
      results << t
      results.find { |x| x[:word]= "#{string}" }
    end

    def position(string, image)
      p = extract_text_from_word("#{string}", "#{image}")
      x, x1, y, y1 =[p[:x_start], p[:x_end], p[:y_start], p[:y_end]]
      c1 = (x + x1)/2
      c2 = (y + y1)/2
      return c1, c2
    end

#find text by ocr
    def find_text_string_on_image(file_name)
      save_screen(file_name)
      sleep 1
      image = RTesseract.new("#{IMAGES_PATH}/#{file_name}.png")
      word = image.to_s
      puts word
      puts file_name
      if word.include? "#{file_name}"
        return true
      else
        return false
      end
    end

    def save_screen(file_name = nil)
      if file_name.nil? || file_name == ''
        current_screen = 'ocr_screen'
      else
        current_screen = file_name
      end
      $driver.driver.screenshot("#{IMAGES_PATH}/#{current_screen}.png")
    end

#touch text by ocr
    def touch_text_by_location(x, y)
      Appium::TouchAction.new.press(x: x, y: y).wait(5).release.perform
    end

    def touch_string_by_ocr(string, image)
      c1, c2 = position(string, image)
      touch_text_by_location(c1, c2)
    end
  end
end




