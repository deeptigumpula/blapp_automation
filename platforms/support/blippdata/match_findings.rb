require "rubygems"
require 'appium_lib'
require 'selenium-webdriver'
require "bundler/setup"
require 'opencv'
require_relative('image_matcher/image_matcher.rb')
require_relative '../../../platforms/support/constants'

include OpenCV

def save_screen(file_name = nil)
  if file_name.nil? || file_name == ''
    current_screen = 'current_screen.png'
  else
    current_screen = file_name
  end
  $driver.driver.screenshot("#{IMAGES_PATH}/#{current_screen}")
end

def findbyImg(templatefile, screenfile)
  template_file_name = "#{IMAGES_PATH}/#{templatefile}"
  match_image_filename = "#{IMAGES_PATH}/#{screenfile}"
  template = CvMat.load(template_file_name).BGR2GRAY
  match_image = CvMat.load(match_image_filename).BGR2GRAY
  result = match_image.match_template(template, :sqdiff_normed)
  res = result.normalize.min_max_loc
  pt1 = result.min_max_loc[2] # minimum location
  pt2 = CvPoint.new(pt1.x + template.width, pt1.y + template.height)
  center1 = pt1.x + (template.width)/2
  center2 = pt1.y + (template.height)/2
  return center1, center2
end

def findbyImgcolor(templatefile, screenfile)
  template_file_name = "#{IMAGES_PATH}/#{templatefile}"
  match_image_filename = "#{IMAGES_PATH}/#{screenfile}"
  template = CvMat.load(template_file_name)
  match_image = CvMat.load(match_image_filename)
  result = match_image.match_template(template, :sqdiff_normed)
  res = result.normalize.min_max_loc
  pt1 = result.min_max_loc[2] # minimum location
  pt2 = CvPoint.new(pt1.x + template.width, pt1.y + template.height)
  center1 = pt1.x + (template.width)/2
  center2 = pt1.y + (template.height)/2
  return center1, center2
end

def touch_by_x_y(x, y)
  puts "tapping on button"
  sleep 2
  #Appium::TouchAction.new.press(x:x, y:y).wait(5).release.perform
  Appium::TouchAction.new.tap(x: x, y: y, fingers:1).release.perform
end



def touch_image_screen(template_file)
  sleep 10
  save_screen
  x, y = findbyImg(template_file, 'current_screen.png')
  if x != nil
    touch_by_x_y(x, y)
    true
  else
    false
  end
end

def get_location(template_file)
  save_screen
  x, y = findbyImgcolor(template_file, 'current_screen.png')
  return x,y
end

def touch_or_image_screen(template_file)
  save_screen
  x, y = findbyImgcolor(template_file, 'current_screen.png')
  if x != nil
    touch_by_x_y(x, y)
    true
  else
    false
  end
end

def find_temp(template_file, screen_file, match_level)
  template_file_name = "#{IMAGES_PATH}/#{template_file}"
  template = CvMat.load(template_file_name).BGR2GRAY

  screen_file_name = "#{IMAGES_PATH}/#{screen_file}"
  match_image = CvMat.load(screen_file_name).BGR2GRAY
  result = match_image.match_template(template, :sqdiff_normed)

  find = result.normalize.min_max_loc
  if find[0] <= match_level
    return true
  end
end

def find_temp_color(template_file, screen_file, match_level)
  template_file_name = "#{IMAGES_PATH}#{template_file}"
  template = CvMat.load(template_file_name)
  screen_file_name = "#{IMAGES_PATH}/#{screen_file}"
  match_image = CvMat.load(screen_file_name)
  result = match_image.match_template(template, :sqdiff_normed)
  find = result.normalize.min_max_loc
  if find[0] <= match_level
    return true
  end
end

def or_image_exists(template_file)
  sleep 2
  save_screen
  find_temp_color(template_file, 'current_screen.png', 0.00001)
end

def image_exists(template_file)
  save_screen
  sleep 2
  find_temp(template_file, 'current_screen.png', 0.00001)
end

def check_image(template_file_name, current_screen)
  template = "#{IMAGES_PATH}/#{template_file_name}"
  match_image_filename = "#{IMAGES_PATH}/#{current_screen}"
  template = CvMat.load(template)
  match_image = CvMat.load(match_image_filename)
  result = match_image.match_template(template, :sqdiff_normed)
  res = result.normalize.min_max_loc
  t = result.normalize.min_max_loc[0]
  puts t
  puts "true" if t < 0.0001
  pt1 = result.min_max_loc[2] # minimum location
  pt2 = CvPoint.new(pt1.x + template.width, pt1.y + template.height)
  center1 = pt1.x + (template.width)/2
  center2 = pt1.y + (template.height)/2
  puts "c1 is:#{center1} and c2 is :#{center2}"
end
