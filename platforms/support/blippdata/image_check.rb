=begin
require 'opencv'
require "json"
require "httpclient"
require "chunky_png"
require 'RMagick'
require 'rtesseract'
require 'open-uri'
require_relative '../../../platforms/support/constants'
include OpenCV


puts "Enter template image name"
template = gets.chomp

puts "Enter original image name"
match_image_filename = gets.chomp

template = CvMat.load(template)
match_image = CvMat.load(match_image_filename)
result = match_image.match_template(template, :sqdiff_normed)
res = result.normalize.min_max_loc
t = result.normalize.min_max_loc[0]
puts t
puts "true" if t < 0.0001
pt1 = result.min_max_loc[2] # minimum location
pt2 = CvPoint.new(pt1.x + template.width, pt1.y + template.height)
center1 = pt1.x + (template.width)/2
center2 = pt1.y + (template.height)/2
puts "c1 is:#{center1} and c2 is :#{center2}"

match_image.rectangle!(pt1, pt2, :color => CvColor::Black, :thickness => 3)
window = GUI::Window.new('Display window') # Create a window for display.
window.show(match_image) # Show our image inside it.
GUI::wait_key
=end


