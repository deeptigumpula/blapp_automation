require 'open-uri'
require 'chunky_png'
image_url = "http://res.cloudinary.com/dpatglqbi/image/upload/v1476100040/Cube.png"
image_file = open(image_url).read
image = ChunkyPNG::Image.from_blob(image_file)
def img(image_name)
  download = open("http://res.cloudinary.com/dpatglqbi/image/upload/v1476100040/#{image_name}")
  IO.copy_stream(download, "#{PROJECT_ROOT}/features/android/support/images/#{image_name}")
end

