require_relative 'match_findings'
def find_match_exists(template_file, screen_file)
  search_image_path ="#{IMAGES_PATH}/#{screen_file}"
  template_image_path ="#{IMAGES_PATH}/#{template_file}"
  im = ImageMatcher.new
  im.search_image = search_image_path
  im.template_image = template_image_path
  im.verbose = true
  im.strategy = 'opencv'
  im.fuzz = 0.0
  im.highlight_match = true
  if im.has_match?
    #puts "\nYes. Matches at: " + im.match_result.join(",")
    return 'true'
  else
    return 'false '
  end
end

def find_image_location(template_file, screen_file)
  search_image_path ="#{IMAGES_PATH}/#{screen_file}"
  template_image_path ="#{IMAGES_PATH}/#{template_file}"
  im = ImageMatcher.new
  im.search_image = search_image_path
  im.template_image = template_image_path
  im.verbose = true
  im.strategy = 'opencv'
  im.fuzz = 0.0
  im.highlight_match = true
  im.match!
  x, y = im.match_result
  w = im.template_image.columns/2
  h = im.template_image.rows/2
  center1 = x + w
  center2 = y + h
  return center1, center2
end


def image_match_exists(template_file)
  save_screen
  find_match_exists(template_file, 'current_screen.png')
end

def touch_by_location(x, y)
  puts "tapping on button"
  Appium::TouchAction.new.press(x: x, y: y).wait(5).release.perform
end


def touch_image_by_location(template_file)
  sleep 10
  save_screen
  x, y = find_image_location(template_file, 'current_screen.png')
  if x != nil
    touch_by_location(x, y)
    true
  else
    false
  end
end

def match_exists(template_file_name)
  save_screen
  get_image_data(template_file_name, 'current_screen.png')
end

def get_image_data(template_file_name, current_screen)
  template = "#{IMAGES_PATH}/#{template_file_name}"
  match_image_filename = "#{IMAGES_PATH}/#{current_screen}"
  template = CvMat.load(template)
  match_image = CvMat.load(match_image_filename)
  result = match_image.match_template(template, :sqdiff_normed)
  res = result.normalize.min_max_loc
  t = result.normalize.min_max_loc[0]
  if (t < 0.0001) then
    return true
  else
    return false
  end
end

def get_position
  template = "#{IMAGES_PATH}/#{template_file_name}"
  match_image_filename = "#{IMAGES_PATH}/#{current_screen}"
  template = CvMat.load(template)
  match_image = CvMat.load(match_image_filename)
  result = match_image.match_template(template, :sqdiff_normed)
  res = result.normalize.min_max_loc
  t = result.normalize.min_max_loc[0]
  pt1 = result.min_max_loc[2] # minimum location
  pt2 = CvPoint.new(pt1.x + template.width, pt1.y + template.height)
  center1 = pt1.x + (template.width)/2
  center2 = pt1.y + (template.height)/2
  return center1, center2
end


