require_relative '../android/support/operations/launch_menus'

Given(/^I am on default lens screen$/) do
  @scanningpage = launch_scanning_screen
  @scanningpage.verify_default_screen
  @code_label_exists = @scanningpage.code_tab_exists
end


And(/^I go to settings screen$/) do
  @settings_page = @scanningpage.launch_settings_screen
  tab = @code_tab_exists ? "Enter a code" : "Change code"
  @settings_page.scroll_to_enter_code(tab)
end

Then(/^I verify blipp "([^"]*)" is loaded successfully$/) do |blipp_name|
  Assertions.assert_true(check_blipp_loaded(blipp_name),'The Blipp is not loaded successfully.Please check the loading of blipp')
end

Given(/^I trigger the "([^"]*)" blipp$/) do |blipp_name|
  @scanningpage = launch_scanning_screen
  @scanningpage.verify_default_screen
  @code_label_exists = @scanningpage.code_tab_exists
  @settings_page = @scanningpage.launch_settings_screen
  tab = @code_tab_exists ? "Change code" : "Enter a code"
  @settings_page.scroll_to_enter_code(tab)
  @settings_page.enter_blipp_code(blipp_name)
  @blipp_name = blipp_name
end