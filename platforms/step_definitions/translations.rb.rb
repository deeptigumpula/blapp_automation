Given(/^I change language to "([^\"]*)"$/) do |language|
  @language_set = language
  case language
    when 'turkish'
      change_language(Android::Language::TURKISH, Android::Locale::TURKEY)
    when 'chinese'
      change_language(Android::Language::CHINESE, Android::Locale::CHINA)
    when 'japanese'
      change_language(Android::Language::JAPANESE, Android::Locale::JAPAN)
    when 'croatian'
      change_language(Android::Language::CROATIAN, Android::Locale::CROATIA)
  end
  $driver.driver.restart
end

When(/^I tap on arrow button$/) do
  @page = page(HomePage)
  @page.tap_on_arrow
end

def change_language(language, locale)
  system("adb -s #{$device_id} shell 'setprop persist.sys.language #{language}; setprop persist.sys.country #{locale}; setprop ctl.restart zygote'")
end

def change_language_to(language)
  sleep 10
  $driver.driver_quit
  sleep 2

  caps = Appium.load_appium_txt file: File.expand_path("#{PROJECT_ROOT}/features/android/config/#{language}/appium.txt", __FILE__), verbose: true

  @appium_driver = Appium::Driver.new(caps)
  Appium.promote_appium_methods self.class

  @selenium_driver = @appium_driver.start_dr
  sleep 10
end


