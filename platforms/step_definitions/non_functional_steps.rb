Then(/^I get the battery usage of the app after launch$/) do
  connected_devices_battery_info
end

Then(/^I get the memory usage of the app after launch$/) do
  get_memory_usage_of_app
end

Then(/^I get the cpu information of the app after launch$/) do
  get_cpu_information
end