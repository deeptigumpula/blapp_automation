require_relative '../../platforms/support/blippdata/match'

And(/^I should see "([^"]*)"$/) do |template_file|
  match_exists(template_file)
end


Then(/^I verify the browser is opened within the app$/) do
  puts match_exists("WebView.png")
end