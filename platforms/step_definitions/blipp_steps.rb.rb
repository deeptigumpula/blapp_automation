require_relative '../../platforms/android/support/operations/launch_menus'
When(/^I tap on the "([^"]*)" button$/) do |share_option|
  @page = page(BlippPageAndroid).await
  case share_option
    when "Share Blipp"
      @page.tap_on_share_button
    when "Share Photo"
      @page.tap_share_photo_icon
      @page = page(BlippPhotoPageAndroid).await
    when "Save to Device"
      sleep 10
      @page.tap_share_photo_icon
      @page = page(BlippPhotoPageAndroid).await
      @page.tap_on_save_to_device_button
      @page = page(PhotoSavedAlertPage).await
      end
end

Then(/^I should see all the icons Facebook, Twitter and Email$/) do
  @page.verify_social_share_icons
end

When(/^I select "([^"]*)" icon$/) do |social_share_option|
  case social_share_option
    when "Facebook"
      @page.tap_on_facebook_icon
    when "Twitter"
      @page.tap_on_twitter_icon
    when "Instagram"
      @page.tap_on_instagram_icon
    when "Email"
      @page.tap_on_email_icon
      @page = page(GmailPage).await
  end
end

Then(/^I verify "([^"]*)" share pop up appears$/) do |social_option|
  @page.share_pop_up_exists(social_option)
end

When(/^I fill "([^"]*)" in pre\-fill box of "([^"]*)" share pop up$/) do |text, social_icon|
  @page.enter_status_text(text, social_icon)
  $status = text
end

And(/^I tap on Post button of "([^"]*)" share pop up$/) do |social_option|
  @page.tap_on_post_button(social_option)
  t= Time.now
  $time = t.strftime("%I:%M%p").split("A").first
end


Then(/^I verify blipp is shared on Facebook$/) do
  @page.verify_blipp_shared_on_facebook($status, $time)
end

Then(/^I verify (?:blipp|photo) is shared on "([^"]*)"$/) do |social_share_option|
  case social_share_option
    when "Facebook"
      @page.verify_blipp_shared_on_facebook($status, $time)
    when "Twitter"
      @page.verify_message_on_twitter($status)
  end
end


Then(/^I verify that same photo of "([^"]*)" blipp is saved to device gallery$/) do |photo_name|
  @page.verify_photo_saved_alert
  launch_gallery_app
  Assertions.assert_true( match_exists("#{photo_name}.png"),'Same photo is not saved in gallery.Please check manually')
end


Then(/^I verify that photo appears on Instagram$/) do
 @page.verify_photo_posted_on_instagram($blipp_name)
  puts "Photo verification on instagram is completed"
end

Then(/^I verify Instagram share screen appears$/) do
  @page.verify_instagram_share_screen_appears
end

When(/^I fill "([^"]*)" in pre\-fill box of Instagram screen$/) do |blipp_name|
  $blipp_name = blipp_name
  @page.fill_in_text_in_instagram_pop_up(blipp_name)
end

And(/^I tap on the Instagram share button$/) do
  @page.tap_on_insta_share_button
end


And(/^I type in "([^"]*)"$/) do |email|
 @shared_text = @page.type_in_email(email)
end

And(/^I tap on send button$/) do
  @page.tap_on_send_button
end

Then(/^I verify email is recieved$/) do
 @page.verify_email_recieved(@shared_text)
end


And(/^I tap on character "([^"]*)"$/) do |arg|
  touch_image_screen("#{arg}.png")
end

And(/^I tap on "([^"]*)" button$/) do |arg|
  touch_image_screen("#{arg}.png")
end