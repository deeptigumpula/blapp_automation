module Android
  module Language
    TURKISH = 'tr'
    ENGLISH = 'en'
    CHINESE = 'zh'
    FRENCH= 'fr'
    CROATIAN = 'hr'
    JAPANESE = 'ja'
  end
end


module Android
  module Locale
    TURKEY = 'TR'
    US= 'US'
    CHINA = 'CN'
    BRITAIN= 'GB'
    CANADA ='CA'
    JAPAN = 'JP'
    CROATIA = 'HR'
  end
end

