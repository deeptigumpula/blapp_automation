require_relative '../support/mwebbase'
require_relative '../support/test_helper_mweb'
module LaunchMenu
  def lens_selector
    lens_selector = {id: 'bt_home_lens_selector'}
    check_element_exists(lens_selector)
    touch(lens_selector)
    @page = page(LensSelectorPage).await
  end
end

